//
//  InterfaceController.swift
//  Activity3 WatchKit Extension
//
//  Created by Melvin John on 2019-11-04.
//  Copyright © 2019 Melvin John. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    
    
    @IBOutlet weak var cityName: WKInterfaceLabel!
    
    
    @IBOutlet weak var chooseCity: WKInterfaceButton!
    
    var userResponse: String!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
        // 1. Check if the watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    @IBAction func selectCity() {
        let suggestedResponses = ["Toronto", "Rome", "Auckland"]
        presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) { (results) in
            
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                self.userResponse = results?.first as? String
                
                self.cityName.setText(self.userResponse!)
                
                self.sendTheCityNameToPhone(city: self.userResponse)
            }
        }
    }
    
    func sendTheCityNameToPhone(city: String) {
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            WCSession.default.sendMessage(
                ["city" : city],
                replyHandler: nil)
        }
        else {
            print("Phone is not reachable")
        }
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
}
