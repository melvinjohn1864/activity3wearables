// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

InternetButton button = InternetButton();

int hourInt;
int minuteInt;
int lightOfminute;
int twelveHour;
int temp;
int tempCelsius;
int noOfLights;

void setup() {
    
    button.begin();
    
    Particle.function("timeHour", showHour);
    Particle.function("timeMinute", showMinute);
    Particle.function("temperatureCelcius", showTemperature);

}

int showHour(String cmd){
    hourInt = cmd.toInt();
   if(hourInt != 12){
       if (hourInt > 12){
           twelveHour = hourInt - 12;
           button.ledOn(twelveHour,255,0,0);
       }else {
           button.ledOn(hourInt,255,0,0);
       }
   }
    return 1;
}

int showMinute(String cmd){
    minuteInt = cmd.toInt();
    lightOfminute = minuteInt/5;
    if (hourInt > 12){
        if (lightOfminute != twelveHour){
            button.ledOn(lightOfminute,255,255,255);
            delay(4000);
            button.allLedsOff();
        }else {
            button.ledOn(lightOfminute,255,0,0);
            delay(4000);
            button.allLedsOff();
        }
    }else {
        if (lightOfminute != hourInt){
            button.ledOn(lightOfminute,255,255,255);
            delay(4000);
            button.allLedsOff();
        }else {
            button.ledOn(lightOfminute,255,0,0);
            delay(4000);
            button.allLedsOff();
        }
    }
    return 1;
}


int showTemperature(String cmd){
    temp = cmd.toInt();
    tempCelsius = temp - 273.15;
    
    if(tempCelsius >= -30 && tempCelsius < -20){
        noOfLights = 1;
    }else if(tempCelsius >= -20 && tempCelsius < -10){
        noOfLights = 2;
    }else if(tempCelsius >= -10 && tempCelsius < 0){
        noOfLights = 3;
    }else if(tempCelsius >= 0 && tempCelsius < 10){
        noOfLights = 4;
    }else if(tempCelsius >= 10 && tempCelsius < 20){
        noOfLights = 5;
    }else if(tempCelsius >= 20 && tempCelsius < 30){
        noOfLights = 6;
    }
    
    for(int i = 0; i < noOfLights; i++){
        button.ledOn(i,255,255,255);
        delay(200);
    }
    
    delay(3000);
    button.allLedsOff();
}


 int DELAY = 200;
void loop() {
    
    if (button.buttonOn(2)) {
    // CHOICE = B
    Particle.publish("timeAndWeather","time", 60, PRIVATE);
    delay(DELAY);
  }

  if (button.buttonOn(3)) {
      // CHOICE = C
    Particle.publish("timeAndWeather","temperature", 60, PRIVATE);
    delay(DELAY);
  }

}