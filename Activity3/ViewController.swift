//
//  ViewController.swift
//  Activity3
//
//  Created by Melvin John on 2019-11-04.
//  Copyright © 2019 Melvin John. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import WatchConnectivity
import Particle_SDK

class ViewController: UIViewController, WCSessionDelegate {
    
    let USERNAME = "melvinjohn1864@gmail.com"
    let PASSWORD = "Melvin@1864"
    
    let DEVICE_ID = "25003d000447363333343435"
    var myPhoton : ParticleDevice?
    
    var API_KEY = "67f508dd84bd4c197946c901ce7f3349"
    var cityName:String = ""
    var timeZone:String = ""
    var currentTime = ""
    
    var hour: String!
    var minute: String!
    var tempString:String!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if WCSession.isSupported() {
            print("\nPhone supports WCSession")
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        else {
            print("Phone does not support WCSession")
        }
        
        // 1. Initialize the SDK
        ParticleCloud.init()
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                
                // try to get the device
                self.getDeviceFromCloud()
                
            }
        }
    }
    
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                
                self.subscribeToParticleEvents()
                //self.subscribeToXMotion()
            }
            
        }
    }
    
    func subscribeToParticleEvents() {
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
            withPrefix: "timeAndWeather",
            deviceID:self.DEVICE_ID,
            handler: {
                (event :ParticleEvent?, error : Error?) in
                
                if let _ = error {
                    print("could not subscribe to events")
                } else {
                    print("got event with data \(event?.data)")
                    let choice = (event?.data)!
                    
                    if (choice == "time") {
                        self.showTimeOnParticle()
                    }else if (choice == "temperature") {
                        self.showTempOnParticle()
                    }
                }
        })
    }
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        DispatchQueue.main.async {
            
            let city = message["city"] as! String
            
            if (city == "Toronto"){
                self.timeZone = "America/Toronto"
            }else if(city == "Rome"){
                self.timeZone = "Europe/Rome"
                
            }else if(city == "Auckland"){
                self.timeZone = "Pacific/Auckland"
            }
            
            self.getWeatherInfo(cityName: city)
            self.getCurrentTime()
        }
        
    }
    
    
    func getWeatherInfo(cityName: String) {
        print("Called weather data API")
        let URL = "https://api.openweathermap.org/data/2.5/weather?q=\(cityName)&appid=\(API_KEY)"
        print(URL)
        Alamofire.request(URL).responseJSON {
            (currentWeather) in
            print(currentWeather.value)
            
            // convert the response to a JSON object
            
            let jsonResponse = JSON(currentWeather.value)
            //let main = jsonResponse["name"]
            let description = jsonResponse["weather"]
            let temp = jsonResponse["main"]["temp"]
            let desc = description[0]["description"]
            
            let temperatureInKelvin = jsonResponse["main"]["temp"]
            
            self.tempString = temperatureInKelvin.description
            
            
    
            
        }
    }
    
    func getCurrentTime(){
        print("Called world time  API")
        let URL = "https://worldtimeapi.org/api/\(self.timeZone)/"
        //print(URL)
        Alamofire.request(URL).responseJSON {
            (currentTimeReceived) in
            print(currentTimeReceived)
            let responseTime = JSON(currentTimeReceived.value)
            let dateTime = responseTime["datetime"]
            print("Date time received \(dateTime)")
            self.getHourAndMinute(time: dateTime.string!)
        }
    }
    
    func getHourAndMinute(time: String){
        var time = time.components(separatedBy: "T")
        var timeData = time[1].components(separatedBy: ".")
        var hourData = timeData[0].components(separatedBy: ":")
        hour = hourData[0]
        minute = hourData[1]
        print("The hour data is \(hour)")
        print("The minute data is \(minute)")
    }
    
    func showTimeOnParticle() {
        
        print("Pressed the change lights button")
        
        let hourParameters = [hour]
        let minuteParameters = [minute]
        var task = myPhoton!.callFunction("timeHour", withArguments: hourParameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
        
        var task1 = myPhoton!.callFunction("timeMinute", withArguments: minuteParameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
        
    }
    
    
    func showTempOnParticle() {
        
        print("Pressed the change lights button")
        
        let parameters = [tempString]
        var task = myPhoton!.callFunction("temperatureCelcius", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
        
    }


}

